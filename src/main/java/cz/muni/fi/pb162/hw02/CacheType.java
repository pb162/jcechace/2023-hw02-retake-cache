package cz.muni.fi.pb162.hw02;

/**
 *
 * @author Jakub Cechacek
 */
public enum CacheType {
    LRU,
    MRU,
    FIFO,
    RANDOM
}
