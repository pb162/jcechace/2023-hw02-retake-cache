package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.Cache;
import cz.muni.fi.pb162.hw02.CacheFactory;
import cz.muni.fi.pb162.hw02.CacheType;

/**
 * Factory class for caches;
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class DefaultCacheFactory<K, V> implements CacheFactory<K, V> {

    @Override
    public Cache<K, V> create(CacheType type, int size) {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
